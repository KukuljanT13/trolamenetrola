__author__ = 'Teo&Katja'
# -*- coding: utf-8 -*-

import requests as r
import re
from  time import *
from _datetime import datetime
from datum import *

def odstejCasa(hvstopna, hizstopna):
    urai = int(hizstopna.split(':')[0])
    urav = int(hvstopna.split(':')[0])
    mini = int(hizstopna.split(':')[1])
    minv = int(hvstopna.split(':')[1])
    if mini<minv:
        mini = mini + 60
        urai = urai - 1
        minute = mini - minv
    else:
        minute = mini - minv
    if urai<urav:
        urai += 24
        ura = urai - urav
    else:
        ura = urai - urav
    if ura<10:
        uraNiz = '0' + str(ura)
    else:
        uraNiz = str(ura)
    if minute<10:
        minuteNiz = '0' + str(minute)
    else:
        minuteNiz = str(minute)
    return uraNiz + ':' + minuteNiz

def sestejCasa(hpostaja1, hpostaja2):
    ura1 = int(hpostaja2.split(':')[0])
    ura2 = int(hpostaja1.split(':')[0])
    min1 = int(hpostaja2.split(':')[1])
    min2 = int(hpostaja1.split(':')[1])

    minute = (min1 + min2)%60
    ura = (ura1 + ura2 + (min1 + min2)//60)%24

    if ura<10:
        uraNiz = '0' + str(ura)
    else:
        uraNiz = str(ura)
    if minute<10:
        minuteNiz = '0' + str(minute)
    else:
        minuteNiz = str(minute)
    return uraNiz + ':' + minuteNiz

class Cas:
    def __init__(self, niz):
        uporabno = niz.split(' ')[::-1]
        niz = uporabno[0]
        self.fensiBus = len(uporabno)==2
        seznam = niz.split(':')
        self.ura = int(seznam[0])
        self.minute = int(seznam[1])
        self.niz = niz

    def __add__(self, other):
        return Cas(sestejCasa(self.niz, other.niz))

    def __sub__(self, other):
        return Cas(odstejCasa(other.niz, self.niz))

    def __lt__(self, other):
        if self.ura < other.ura:
            return True
        elif self.ura > other.ura:
            return False
        else:
            if self.minute < other.minute:
                return True
            else:
                return False

    def __str__(self):
        return self.niz

    def __eq__(self, other):
        return not (self.__lt__(other) or other.__lt__(self))

slovarPostajalisc = dict()
with open("postaje.txt") as f:
    for vrstica in f:
        seznamcek = vrstica.strip().split(':')
        slovarPostajalisc[seznamcek[0].upper()] = seznamcek[1]

preferiranePostaje = ["BAVARSKI DVOR", "HOTEL LEV", "KOLIZEJ", "POŠTA", ]             # referenca za center, oz preferirane prestopne postaje

obračališča = ["BEŽIGRAD", "LETALIŠKA", "ZADOBROVA", "JEŽICA", "NS RUDNIK", "DOLGI MOST P+R"]    # vse postaje ki imajo številko 2 (obračališče = postaja), litostroj je samo obračališče pozor litostroj

pariPostaj = [("SLOVENIJA AVTO", "REMIZA"), ("REMIZA", "ŠIŠENSKA"), ("HOTEL LEV", "KOLIZEJ"), ("KONZORCIJ", "POŠTA"), ("ZALOŠKA", "KAJUHOVA"), ("ORLOVA", "STRELIŠČE"), ("SLOVENIJA AVTO", "ŠIŠENSKA"), ("DALMATINOVA", "TAVČARJEVA"), ("ROGOVILC", "PRIMOŽIČEVA")]
                           # pari postaj, ki imajo drugačno ime na drugi strani ceste (če bo vse po sreči so pari postaj, kjer lahko prideš hitro peš iz ene do druge
istovetnePostaje = dict()

for p1, p2 in pariPostaj:
    if p1 not in istovetnePostaje.keys():
        istovetnePostaje[p1] = []
    if p2 not in istovetnePostaje.keys():
        istovetnePostaje[p2] = []
    istovetnePostaje[p1].append(p2)
    istovetnePostaje[p2].append(p1)

vseLinije = ['1 N', '1 B',"1", '2','3','3 B', '3 N','3G','5', '5 N','6','6 B','7','7 L','8','9','11','11 B','12','13','14',
             '14 B','15','18','18 L','19 B','19 I','19 Z','20','20 Z','21','22','24','25','26','27','27 K','28','29','30']

def obe_smeri_linij(niz):
    seznam=niz.split(':')
    obdelava=[]
    for c in seznam:
        obdelava.append(c.strip())
    obdelava2=obdelava[2].split(' - ')
    vrni1=[obdelava[0], obdelava[1]]
    vrni2=[obdelava[0], obdelava[1]]
    for c in obdelava2:
        c=c.strip()
        if '/' not in c:
            vrni1.append(c)
            vrni2.append(c)
        else:
            t=c.split('/')
            if t[0]!=' ' and t[0]!='':
                vrni1.append(t[0])
            if t[1]!=' ' and t[1]!='':
                vrni2.append(t[1])
    smer = ' - '.join(vrni2[1].split(' - ')[::-1])
    vrni2=[vrni2[0]]+[smer]+vrni2[2:][::-1]
    return vrni1, vrni2

def ugotoviLinijo(smerBusa):
    ime = smerBusa.split(' ')
    if len(ime)>1 and len(ime[1])==1:
        linija = ' '.join(ime[:2])
    else:
        linija = ime[0]
    return linija

slovarLinij = dict()                            # slovar: {"22 FUŽINE P+R - KAMNA GORICA" : [POSTAJE V ENO SMER], "22 KAMNA GORICA - FUŽINE" : [...], ...}
slovar = dict()                                 # SLOVAR: {"20 Z" : set[VSE POSTAJE OD TE LINIJE]}
mnozicaPostaj = set()
with open("linije_naRoke.txt") as f:
    f.readline()
    f.readline()
    for vrstica in f:
        s1, s2 = obe_smeri_linij(vrstica.strip())
        slovarLinij[s1[0].upper()+' '+s1[1].upper()] = [x.upper() for x in s1[2:]]
        slovarLinij[s2[0].upper()+' '+s2[1].upper()] = [x.upper() for x in s2[2:]]
        slovar[ugotoviLinijo(s1[0].upper())] = set([x.upper() for x in s1[2:]] + [x.upper() for x in s2[2:]])
        mnozicaPostaj |= set([x.upper() for x in s1[2:]] + [x.upper() for x in s2[2:]])

def trenutniCas():
    return Cas(str(datetime.now())[11:16])

def leksikografskaRazdalja(beseda1, beseda2):                  # zadovoljivo hitro deluje na nizih dolžine <= 8
    if beseda1=='' or beseda2=='':
        return max(len(beseda1),len(beseda2))
    if beseda1==beseda2:
        return 0
    if beseda1[0]==beseda2[0]:
        return leksikografskaRazdalja(beseda1[1:],beseda2[1:])
    return min(leksikografskaRazdalja(beseda1,beseda2[1:]), leksikografskaRazdalja(beseda1[1:],beseda2),leksikografskaRazdalja(beseda1[1:],beseda2[1:]))+1

def siptarskaRazdalja(b1, b2):
    m1 = set(list(b1))
    m2 = set(list(b2))
    return max(len(m1)-len(m1&m2) ,len(m2) - len(m1&m2))

def preveriTipknje(postaja):
    postaja = postaja.upper()
    # if len(postaja)>6:
    #     postaja = postaja[:6]
    razlika = 2000
    for c in mnozicaPostaj:
        zapomni = c
        if len(c)>len(postaja):
            c = c[:len(postaja)]
        trenutna = siptarskaRazdalja(postaja, c)
        if trenutna == razlika:
            popravek.add(zapomni)
        if trenutna < razlika:
            razlika = trenutna
            popravek = {zapomni}
    return popravek

def odreziPrazenNiz(seznam):
    indeks = len(seznam)-1
    while True:
        if seznam[indeks]=="":
            indeks-=1
        else:
            seznam = seznam[:indeks+1]          # naredi pravo rezino brez praznih nizov na koncu (z indeksom prepisuje samo enkrat namesto da vsakič prepisuje
            break
    return seznam

def linijeNaPostaji(postaja):                # vhod mora biti kar vrne funkcija potegniPostajo
    vrni = set()                             # ker je to zamudna varianta, se uporablja samo linijeNaPostaji2
    for c in postaja:
        if len(c[0].split(' '))>1 and len(c[0].split(' ')[1])==1:
            stevilkaLin = ' '.join(c[0].split(' ')[:2])
        elif c[0].split(' ')[0]!='Route':
            stevilkaLin = c[0].split(' ')[0]
        else:
            stevilkaLin = ''
        if stevilkaLin != '':
            vrni.add(stevilkaLin)
    return vrni

def obeSmeri(linije):
    mnozica = []
    for lin in slovarLinij:
        if len(lin.split(' ')[1])==1:
            pravaLinija = ' '.join(lin.split(' ')[:2])
        else:
            pravaLinija = lin.split(' ')[0]
        if linije==pravaLinija:
            mnozica.append((lin, slovarLinij[lin]))
    return mnozica

def linijeNaPostaji2(postaja):
    postaja = postaja.upper()
    vrni = set()
    for k,v in slovarLinij.items():
        if postaja in v:
            vrni.add(ugotoviLinijo(k))
    return vrni

def veljavnaLinija(vstopna, izstopna, linija):
    vstopna = vstopna.upper()
    izstopna = izstopna.upper()
    if linija not in vseLinije:
        return False, None, None, None
    mozniSmeri = obeSmeri(linija)                 # [["22 FUŽINE - KAMNA GOR." , [seznam postaj]], ... ]
    if vstopna in istovetnePostaje:
        vstopnaSeznam = [vstopna] + istovetnePostaje[vstopna]
    else:
        vstopnaSeznam = [vstopna]
    if izstopna in istovetnePostaje:
        izstopnaSeznam = [izstopna] + istovetnePostaje[izstopna]
    else:
        izstopnaSeznam = [izstopna]

    for vstopna in vstopnaSeznam:
        for izstopna in izstopnaSeznam:
            for indeks in range(2):
                if (vstopna in mozniSmeri[indeks][1] and izstopna in mozniSmeri[indeks][1]):
                    a = mozniSmeri[indeks][1].index(vstopna)
                    b = mozniSmeri[indeks][1].index(izstopna)
                    if mozniSmeri[indeks][1].index(vstopna) <=  mozniSmeri[indeks][1].index(izstopna):
                        return True, mozniSmeri[indeks][0], mozniSmeri[indeks][1][a:b+1], vstopna
    return False, None, None, None

def narediRegularniIzraz(niz):
    vrni='['
    for c in niz:
        if c=='-':
            vrni = '[' + c + vrni[1:]
        else:
            vrni+=c.lower()+c.upper()
    vrni+=']+?'
    return vrni

def izberiLinijo(seznamTelargo, smerLinije):
    for k in seznamTelargo:
        if k[0][:10]==smerLinije[:10]:
            return k

def potegniPostajo(imePostaje, stevilkaTrole = None):
    """

    :rtype : list
    """
    imePostaje = imePostaje.upper()
    if stevilkaTrole==None or stevilkaTrole=='':
        stevilkaTrole = ""
    elif type(stevilkaTrole)==type(3):
        stevilkaTrole = [str(stevilkaTrole)]
    elif type(stevilkaTrole)==type([]):
        stevilkaTrole = [str(x) for x in stevilkaTrole]
    elif type(stevilkaTrole) == type(''):
        stevilkaTrole = [x.strip() for x in stevilkaTrole.upper().split(',')]
    elif type(stevilkaTrole)==type(set()):
        stevilkaTrole = list(stevilkaTrole)

    if imePostaje not in slovarPostajalisc.keys():
        return list()
    else:
        sifraPostaje = slovarPostajalisc[imePostaje]

    if imePostaje[-1] == '2':
        imePostaje = imePostaje[:-1]

    iskalniIzraz = narediRegularniIzraz(imePostaje)

    params = {"_VIEWSTATE":"/wEPDwULLTE2MTk2MjY4MTlkZBVx62zRY0P/EKQ2cGaGd+2sXcYM",
        "lb_stationSelect":sifraPostaje,
        "tb_stationNo":imePostaje,
        "tb_routeNo":"",                          # ni pomembno če je kakšna številka ali ne
        "b_submit":"Prikaži"}

    filter = r'<td colspan="\d*">'+iskalniIzraz+r'\s*\W\d*\W.*?</table>'
    regularniIzraz = r'<td class="RouteCol">(.*?)\r\n'
    objekt = r.post("http://bus.talktrack.com/Default.aspx", data=params)
    filtrirano = re.findall(filter, objekt.text, re.DOTALL)
    seznam = re.findall(regularniIzraz, ''.join(filtrirano), re.DOTALL)
    trole = []                  # seznam ki se vrne na koncu

    for k in seznam:
        k=k[:-5]                                  # odreže zadnji </td>
        trola = k.split('</td><td>')
        trola = odreziPrazenNiz(trola)
        ime = trola[0].split(' ')
        if len(ime)>1 and len(ime[1])==1:
            linija = ' '.join(ime[:2])
        else:
            linija = ime[0]
        if (trola[0].split(' ')[0]=='Route' or linija in stevilkaTrole or stevilkaTrole=="" or trola[0] in stevilkaTrole) and (linija in vseLinije or trola[0].split(' ')[0]=='Route') and not ('GARAŽA' in trola[0].split(' ')):                 # vzame samo želene številke, ne upošteva garaže in obdrži ločni stolpec
            trole.append(trola)

    return trole

def izgradiLinijo(stevilka, zacetnoPostajalisce = None):
    """

    :rtype : list
    """
    postajeLinije = []
    for postaja in slovarPostajalisc.keys():
        potegnjenaPostaja = potegniPostajo(postaja, stevilka)
        if len(potegnjenaPostaja)>0:
            postajeLinije.append(postaja)
    return postajeLinije

def koristnaTrola(sez):
    if sez[2]==None:
        return False
    podSez = sez[2]
    if len(podSez)<2:
        return False
    prviPrihod = Cas(podSez[1])
    return prviPrihod - (trenutniCas() - Cas('00:10'))< Cas('24:10')

def izberiPraviCas(sez1, sez2, cas):
    razlika = None
    koncniCasi = [None]*len(sez1)
    for i,c1 in enumerate(sez1):
        for c2 in sez2[i:]:
            seNiIztekla = False
            if Cas(c2) > c1 + Cas(cas):
                koncniCasi[i] = Cas(c2)
                razlika = str(Cas(c2) - c1)
                seNiIztekla = True
                break
        if not seNiIztekla:
            if razlika == None:
                razlika = '00:20'
            koncniCasi[i] = c1 + Cas(razlika)         # razlika bi morala biti vedno definirana
    return koncniCasi

def izracunajCas(sez, slovarPotegnjenih = dict(), korakPostaj = 10):        # sez oblike ["6 DOLGI MOST P+R - ČRNUČE", [DRAMA, POŠTA, BAVARSKI DVOR, ... ], telargo vrstica za to smer (seznam)], slovar = {'postaja' : potegnjenaPostaja, ...}
    if not koristnaTrola(sez):                                             # korakPostaj je nov, zato da se čim bolj natančno določi čas prihoda s čim manj potegovanjem postaj
        return (None, None, dict())                                        # to je pač treba poskušati
    imeLinije = sez[2][0]
    postajeNaPoti = sez[1]
    potegnjenaVstopna = sez[2]
    if potegnjenaVstopna == None:
        return (None, None, dict())
    vsakaPeta = (len(postajeNaPoti) - 2) // korakPostaj
    zacetniCasi = [Cas(x) for x in potegnjenaVstopna[1:]]
    # for x in zacetniCasi:
    #     print(x, end=' ')
    # print()
    koncniCasi = [Cas(x) for x in potegnjenaVstopna[1:]]

    for nTa in range(vsakaPeta + 1):
        indeks = (nTa + 1) * korakPostaj if (nTa + 1) * korakPostaj < len(postajeNaPoti) else len(postajeNaPoti)-1
        prejsniIndeks = (nTa) * korakPostaj
        potegniProsim = postajeNaPoti[indeks]
        if potegniProsim not in slovarPotegnjenih:
            nTaPostaja = potegniPostajo(potegniProsim)
            slovarPotegnjenih[potegniProsim] = nTaPostaja
        else:
            nTaPostaja = slovarPotegnjenih[potegniProsim]
        nTaPostaja = izberiLinijo(nTaPostaja, imeLinije)
        if nTaPostaja==None:
            return (None, None, dict())
        cas = '00:0' + str(int(max(0, (indeks - prejsniIndeks) * 1 - 2)))
        koncniCasi = izberiPraviCas(koncniCasi, nTaPostaja[1:], cas)
    return  zacetniCasi, koncniCasi, slovarPotegnjenih

def lepoNaprintaj(brezPrestopanjaSeznam):
    a = 0
    b = 0
    c = 0
    d = 0
    for x in brezPrestopanjaSeznam:
        if len(str(x[0]))>a:
            a=len(str(x[0]))
    #     if len(str(x[1]))>b:
    #         b=len(str(x[1]))
    #     if len(str(x[2]))>c:
    #         c=len(str(x[2]))
    #     if len(str(x[3]))>d:
    #         d = len(str(x[3]))
    niz = '{0:'+str(a+5)+'} {3:63} {1}'
    for x in brezPrestopanjaSeznam:
        print(niz.format(*tuple([str(y) for y in x])))

def brezPrestopanja(vstopna, izstopna, steviloTrol=3):                  # poskrbi: izračun časa, nov neimenovan parameter čas prihoda
    vstopna = vstopna.upper()
    izstopna = izstopna.upper()

    informacijePovezav = []
    povlectiMorava = set()

    #seznamVstopna = potegniPostajo(vstopna)                       # moraš še poskrbet, da se pravilno izpiše na zaslon!!    ZASTARELA KODA
    #seznamIzstopna = potegniPostajo(izstopna)

    mnozicaV = linijeNaPostaji2(vstopna)                     # najprej preveri in poskrbi v primeru da imata postaji skupno linijo
    mnozicaI = linijeNaPostaji2(izstopna)                    # uporablja množice, saj nam vrstni red ni pomemben
    skupneLinije = set()
    for c in mnozicaV:
        if c in mnozicaI and c in vseLinije:
            skupneLinije.add(c)
    if skupneLinije:
        for l in skupneLinije:
            veljavnost, smerLinije, rezina, pravaVstopnaPostaja = veljavnaLinija(vstopna, izstopna, l)
            if not veljavnost:                   # ker telargo pravi da na nekaterih psotajah ustavijo avtobusi ki sploh ne vozijo tam
                continue
            informacijePovezav.append([smerLinije, rezina])
            povlectiMorava.add(pravaVstopnaPostaja)
        seznamAvtobusov = []
        # mnozicaNeuporabnih = set()
        seznamPrvihTrehTrol = []
        for x in povlectiMorava:
            seznamAvtobusov += potegniPostajo(x)
        for indeks, x in enumerate(informacijePovezav):
            telargoVrstica = izberiLinijo(seznamAvtobusov, x[0])
            x.append(telargoVrstica)
            if telargoVrstica == None:
                x = [trenutniCas() - Cas('23:59')] + x                             # to se uporabi za sortiranje, ker zmanjša časovno zahtevnost in vrne samo prihode prvih treh avtobusov
            else:
                x = [Cas(telargoVrstica[1]) - trenutniCas() + Cas('00:01')]+x
            informacijePovezav[indeks] = x
        informacijePovezav.sort()
        slovarPotegnjenih = dict()
        a=min(steviloTrol, len(informacijePovezav))
        for x in informacijePovezav[:min(steviloTrol, len(informacijePovezav))]:
            del x[0]
            prihodiVstopne, prihodiIzstopne, vrnjenSlovar =  izracunajCas(x, slovarPotegnjenih)
            slovarPotegnjenih.update(vrnjenSlovar)
            if prihodiIzstopne == None:
                # mnozicaNeuporabnih.add(informacijePovezav.index(x))
                continue
            prihodiVstopne = [str(x) for x in prihodiVstopne]
            prihodiIzstopne = [str(x) for x in prihodiIzstopne]
            x.append((prihodiVstopne, prihodiIzstopne))
            seznamPrvihTrehTrol.append(x)
        # for neuporabnez in sorted(list(mnozicaNeuporabnih))[::-1]:
        #     del informacijePovezav[neuporabnez]
        # for x in seznamPrvihTrehTrol:
        #     print(x)
        #     print('{0:35} {1} {2} {3}'.format(*tuple(x)))

        return seznamPrvihTrehTrol

    else:
        return False

def enoPrestopanje(vstopna, izstopna, linija=None):
    vstopna = vstopna.upper()
    izstopna = izstopna.upper()

    mnozicaV = linijeNaPostaji2(vstopna)
    mnozicaI = linijeNaPostaji2(izstopna)

    pariZaPrestopanje = []
    postajeZaPrestopanje = set()

    for c in mnozicaV:
        for k in mnozicaI:
            if slovar[c] & slovar[k]:
                postajeZaPrestopanje |= slovar[c] & slovar[k]
    prestop = None
    for x in preferiranePostaje:
        if x in postajeZaPrestopanje:
            prestop = x
            print("prestopiš lahko na:", prestop)
            break
    if not prestop:
        prestop = postajeZaPrestopanje

    if type(prestop)==type(""):
        for c in mnozicaV:
            for k in mnozicaI:
                if prestop in  slovar[c] & slovar[k]:
                    pariZaPrestopanje.append((c,k))
        print("to lahko storiš na naslednje načine:", pariZaPrestopanje)
    else:
        for c in mnozicaV:
            for k in mnozicaI:
                if slovar[c] & slovar[k]:
                    pariZaPrestopanje.append((c,k))
                    print("če izbereš opcijo:",(c,k),'lahko prestopiš na:', slovar[c] & slovar[k])




#################### TESTNI PROSTOR ####################


#izracunajCas(['25 ZADOBROVA - MEDVODE', ['FRIŠKOVEC', 'KOLODVOR', 'BAVARSKI DVOR', 'HOTEL LEV', 'TIVOLI', 'STARA CERKEV', 'KINO ŠIŠKA', 'REMIZA'], ['25 ZADOBROVA - MEDVODE', 'n 18:01', '18:34', '19:04']])


#['11 JEŽICA P+R - ZALOG', '17:21', 'n 17:38', 'n 17:58']

# for x in izberiPraviCas([ 'n 17:30', 'n 17:50'], ['17:21', 'n 17:38', 'n 17:58'], '00:03', '00:10'):
#     print(x, end=' ')


# for x in potegniPostajo('drama',2):
#     print(x)


# print(preveriTipknje(''))


# a = clock()
# brezPrestopanja('hotel lev', 'bavarski dvor', 3)
# b=clock()
# print(b-a)

#print(veljavnaLinija("stara cerkev", "ajdovščina", "7"))

# a = clock()
# enoPrestopanje("jadranska", "drenikova")
# b=clock()
# print(b-a)

#print(veljavnaLinija("pot na fužine", "bežigrad", "20"))


# print(slovarLinij['6 ČRNUČE - DOLGI MOST P+R'])
#
# a=clock()
# i=0
# smer = '3 N BAVARSKI DVOR - RUDNIK'
# for postaja in slovarLinij[smer]:
#     i+=1
#     if i%1==0:
#         print(postaja , potegniPostajo(postaja, smer), sep='\n')
#         b=clock()
#         print(b-a)
#         a=b
# print(len(slovarLinij[smer]))


#print(slovarPostajalisc["LETALIŠKA2"])

# for k in sorted([x for x in slovarLinij.keys()]):
#     print(k, len(slovarLinij[k]))









# c = potegniPostajo("roška")
# for k in c:
#     print(k)























