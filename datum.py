def jePrestopno(leto):
    if leto%4==0 and leto%100!=0 or leto%400==0:
        return True
    return False

def steviloDni(leto):
    if jePrestopno(leto):
        return 366
    return 365

def dolzineMesecev(leto):
    if jePrestopno(leto):
        return [31,29,31,30,31,30,31, 31, 30, 31,30,31]
    return[31,28,31,30,31,30,31, 31, 30, 31,30,31]

class Datum:
    def __init__(self, dan, mesec, leto):
        self.dan=dan
        self.mesec=mesec
        self.leto=leto
        self.steviloDni = steviloDni(self.leto)

    
    def __repr__(self):
        return str(self.dan)+'. '+str(self.mesec)+'. '+str(self.leto)

    
    def __repr__(self):
        return 'Datum('+str(self.dan)+', '+str(self.mesec)+', '+str(self.leto)+')'

    def jeVeljaven(self):
        if self.mesec>12:
            return False
        if jePrestopno(self.leto):
            if self.mesec==2 and self.dan<=29:
                return True
        else:
            if self.mesec==2 and self.dan<=28:
                return True
        if self.mesec in (1,3,5,7,8,10,12) and self.dan<=31:
            return True
        if self.mesec in (4,6,9,11) and self.dan<=30:
            return True
        return False

    def __lt__(self, drugi):
        a=(self.leto,self.mesec,self.dan)
        b=(drugi.leto,drugi.mesec,drugi.dan)
        if a<b:
            return True
        return False

    def __eq__(self, drugi):
        a=(self.leto,self.mesec,self.dan)
        b=(drugi.leto,drugi.mesec,drugi.dan)
        if a==b:
            return True
        return False

    def danVLetu(self):
        t=dolzineMesecev(self.leto)
        i=0
        for k,c in enumerate(t):
            if k==self.mesec-1:
                break
            i+=c
        return i+self.dan

    def razlika(self, drugi):
        t=0
        if self<drugi:
            manjsi=self
            vecji=drugi
            k=-1
        else:
            manjsi=drugi
            vecji=self
            k=1
        t+=vecji.danVLetu()
        vL=vecji.leto
        while vL!=manjsi.leto:
            vL-=1
            t+=steviloDni(vL)
        
        return (t-manjsi.danVLetu())*k

    def danVTednu(self):
        drugi=Datum(21,10,1994)
        
        k=self.razlika(drugi)%7
        if k<0:
            z=(4-k)%7 +1
        else:
            z=(4+k)%7 +1
        return z

    def tedenVLetu(self):
        danD=Datum(1,1,self.leto)
        d=8-danD.danVTednu()
        t=(self.danVLetu()-d)//7 + 2
        
        return t

    

















    
