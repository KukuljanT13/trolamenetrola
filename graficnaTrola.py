__author__ = 'Teo&Katja'
# -*- coding: utf-8 -*-

from tkinter import *
import telargo

class TrolaMeNeTrola():
    def __init__(self, master):
        barvaGumbov = '#FFFF99'
        razmik = 5
        pojasnilo = StringVar(value='Vnesite vstopno in izstopno postajo.')
        labelPojasnilo = Label(master, textvariable=pojasnilo, bg=barvaGumbov, pady=razmik)
        labelPojasnilo.grid(row=0, column=0, columnspan=9, sticky=E+W)

        napisV = StringVar(value='Vstopna postaja:')
        labelNapisV = Label(master, textvariable=napisV, bg=barvaGumbov, pady=razmik)
        labelNapisV.grid(row=1, column=0)

        napisI = StringVar(value='Izstopna postaja:')
        labelNapisI = Label(master, textvariable=napisI, bg=barvaGumbov, pady=razmik)
        labelNapisI.grid(row=2, column=0)

        napisLinija = StringVar(value='Linija (neobvezno):')
        labelNapisLinija = Label(master, textvariable=napisLinija, bg=barvaGumbov, pady=razmik)
        labelNapisLinija.grid(row=3, column=0)

        self.vstopna = StringVar(value=None)
        vstopna = Entry(master, textvariable=self.vstopna, width=25)
        vstopna.grid(row=1, column=1)
        self.vstopna.trace("w", self.ponastaviOrodnoVrstico)

        self.izstopna = StringVar(value=None)
        izstopna = Entry(master, textvariable=self.izstopna, width=25)
        izstopna.grid(row=2, column=1)
        self.izstopna.trace("w", self.zablokiraj)

        self.linija = StringVar(value=None)
        self.linijaEntry = Entry(master, textvariable=self.linija, width=5)
        self.linijaEntry.grid(row=3, column=1)

        gumbZatrolaj = Button(master, text='Zatrolaj', bg=barvaGumbov, command=self.zatrolaj)
        gumbZatrolaj.grid(row=3, column=2)

        self.orodnaVrstica = StringVar(value='Po pritisku na gumb počakajte nekaj trenutkov za izpis podatkov.')
        orodnaVrsticaLabel = Label(master, textvariable=self.orodnaVrstica, bg=barvaGumbov, pady=razmik)
        orodnaVrsticaLabel.grid(row=4, column=0, columnspan=9, sticky=E+W)

        self.scrollbarX = Scrollbar(master, orient=HORIZONTAL)
        self.scrollbarX.grid(row=10, column=0, columnspan=9, sticky=E+W)

        self.izpis = Text(master, width=195, xscrollcommand=self.scrollbarX.set)
        self.izpis.grid(row=5, column=0, columnspan=9, sticky = E+W)
        self.izpis.config(state=DISABLED)

        canvas = Canvas(master, width=300, height=173)
        canvas.grid(row=1, column=5, columnspan=2,rowspan=3, sticky = E+W)
        canvas.background = PhotoImage(file='ozadje2.png')
        canvas.create_image(0, 0, image=canvas.background, anchor='nw')

        root.bind('<Return>', self.posredna)
        vstopna.focus()

    def ponastaviOrodnoVrstico(self, *args):
        self.orodnaVrstica.set('Po pritisku na gumb počakajte nekaj trenutkov za izpis podatkov.')

    def posredna(self, *args):
        self.orodnaVrstica.set('Za izpis podatkov počakajte nekaj trenutkov.')
        self.zatrolaj()

    def zatrolaj(self, *args):
        self.orodnaVrstica.set('Za izpis podatkov počakajte nekaj trenutkov.')
        self.izpis.config(state=NORMAL)
        self.izpis.delete(1.0, END)
        #self.izpis.insert(INSERT, 'stistnili so me!!!! TROLOLOLOLOLO!\n')    # testni izpis
        if self.izstopna.get() == '':
            vrnjeno = telargo.potegniPostajo(self.vstopna.get(), self.linija.get())
            if vrnjeno == []:
                self.popravi(self.vstopna.get(), self.izstopna.get(), self.linija.get())
            else:
                dolzinaPrvega = 0
                for x in vrnjeno:
                    if len(x[0])>dolzinaPrvega:
                        dolzinaPrvega = len(x[0])
                dolzinaPrvega += 2
                self.izpisiPrvoVrstico(dolzinaPrvega, 0, 0)
                for x in vrnjeno:
                    if x[0]=='Route':
                        self.izpis.insert(INSERT, '\n\n')
                        continue
                    self.izpis.insert(INSERT, x[0]+' '*(dolzinaPrvega-len(x[0])))
                    for nizCas in x[1:]:
                        self.izpis.insert(INSERT, str(telargo.Cas(nizCas))+' ')
                    self.izpis.insert(INSERT, '\n\n')
        else:
            vrnjeno = telargo.brezPrestopanja(self.vstopna.get(), self.izstopna.get(), 100)
            a=3
            if vrnjeno == False:
                self.popravi(self.vstopna.get(), self.izstopna.get(), self.linija.get())
            else:
                dolzinaPrvega = 0
                for x in vrnjeno:
                    if len(x[0])>dolzinaPrvega:
                        dolzinaPrvega = len(x[0])
                dolzinaPrvega += 2
                dolzinaDrugega = len('Prihodi na :') + len(self.vstopna.get()) + 2
                dolzinaTretjega = len('Predviden izstop na :') + len(self.izstopna.get()) + 2
                self.izpisiPrvoVrstico(dolzinaPrvega, dolzinaDrugega, dolzinaTretjega)
                self.izpis.insert(INSERT, '\n')
                for x in vrnjeno:
                    self.izpis.insert(INSERT, x[0]+' '*(dolzinaPrvega-len(x[0])))
                    trenutno = 0
                    for nizCas in x[3][0]:
                        self.izpis.insert(INSERT, str(telargo.Cas(nizCas))+' ')
                        trenutno += len(str(telargo.Cas(nizCas))+' ')
                    self.izpis.insert(INSERT, ' '*(dolzinaDrugega - trenutno))
                    trenutno = 0
                    for nizCas in x[3][1]:
                        self.izpis.insert(INSERT, str(telargo.Cas(nizCas))+' ')
                        trenutno += len(str(telargo.Cas(nizCas))+' ')
                    self.izpis.insert(INSERT, ' '*(dolzinaTretjega - trenutno))
                    for postaja in x[1][:-1]:
                        self.izpis.insert(INSERT, postaja + ', ')
                    self.izpis.insert(INSERT, x[1][-1])
                    self.izpis.insert(INSERT, '\n\n')

        self.izpis.config(state=DISABLED)
        self.orodnaVrstica.set('')

    def zablokiraj(self, *args):
        if self.izstopna.get() != '':
            self.linija.set('')
            self.linijaEntry.config(state=DISABLED)
        else:
            self.linijaEntry.config(state=NORMAL)
        self.orodnaVrstica.set('Po pritisku na gumb počakajte nekaj trenutkov za izpis podatkov.')

    def ponudiMoznosti(self, postaja):
        self.izpis.insert(INSERT, "Postaja "+ postaja +" ne obstaja. Ali ste morda mislili:\n\n")
        for popravek in telargo.preveriTipknje(postaja):
            self.izpis.insert(INSERT, popravek+'   ')
        self.izpis.insert(INSERT, '\n\n\n')

    def popravi(self, vstopna, izstopna, linija):
        vstopna = vstopna.upper()
        izstopna = izstopna.upper()
        if izstopna=='':
            if vstopna not in telargo.mnozicaPostaj:
                self.ponudiMoznosti(vstopna)
        else:
            if vstopna not in telargo.mnozicaPostaj:
                self.ponudiMoznosti(vstopna)
            if izstopna not in telargo.mnozicaPostaj:
                self.ponudiMoznosti(izstopna)
            if vstopna in telargo.mnozicaPostaj and izstopna in telargo.mnozicaPostaj:
                self.izpis.insert(INSERT, 'Brezplačna verzija ne vključuje opcije "prestop".')


    def izpisiPrvoVrstico(self, dolzinaStolpca, dolzinaDrugega, dolzinaTretjega, vstop=None, izstop=None):
        if vstop == None:
            vstop = self.vstopna.get()
        if izstop == None:
            izstop = self.izstopna.get()
        niz1 = 'Linije:'+' '*(dolzinaStolpca-8) + 'Prihodi na '+vstop+':'
        niz2 = ' '*dolzinaStolpca + '1.    2.    3.'
        if izstop != '':
            niz1 += ' '*2 + 'Predviden izstop na '+izstop+':' + ' '*2 + 'Postaje, ki jih boste prevozili:'
            niz2 += ' '*(dolzinaDrugega - 14) + niz2.strip() + ' '*(dolzinaTretjega - 14)

        self.izpis.insert(INSERT, niz1 + '\n' + niz2 + '\n')





root = Tk()
root.configure(background='green')
root.title('Trola me ne trola - Ljubljana')
TrolaMeNeTrola(root)
root.mainloop()





























